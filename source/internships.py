import gspread
from oauth2client.service_account import ServiceAccountCredentials
#import pprint
import matplotlib.pyplot as plt
import numpy as np

def read_sheet():

    scope =  ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
    client = gspread.authorize(creds)
    #pp = pprint.PrettyPrinter()

    return client.open('Internships').worksheet('Sheet2')
    #return client.open('Internships').sheet1


def plot(sheet):
    #result = sheet.cell(2, 2).value

    length = len(sheet.col_values(3))

    col_offered_interview = sheet.col_values(4)
    col_interviewed = sheet.col_values(5)
    col_offered = sheet.col_values(6)
    col_denied = sheet.col_values(7)

    offered = 0
    denied = 0
    no_response = 0
    denied_before = 0

    offered_interview = 0
    declined_interview = 0
    denied_after = 0

    for i in range(length):

        if(col_denied[i] == '1' and col_interviewed[i] == '1'):
            denied_after+=1

        if(col_offered[i] == "1"):
            offered+=1
        #elif(col_denied[i] == "1"):
        #    denied+=1
        elif(col_offered_interview[i] != col_interviewed[i] and i > 0):
            #print(i)
            declined_interview+=1

        if(col_offered_interview[i] == '0' and col_denied[i]=='0'):
            no_response+=1

        if(col_offered_interview[i] == "1"):
            offered_interview+=1

        if(col_interviewed[i] == "0" and col_denied[i] == '1'):
            denied_before+=1

    #==============================================================================

    plt.rcParams['toolbar'] = 'None'

    fig = plt.figure(0)
    fig.canvas.set_window_title('Internship Applications')
    #plt.title("Internships")

    #group colors
    o1 = (244, 177, 113)    #No Response
    #o2 = (238, 192, 67)     #Offered Interview
    o2 = (108, 167, 84)     #Offered Interviewed
    o3 = (222, 103, 104)    #Denied
    o4 = (255, 255, 255)

    i1 = (244, 177, 113)    #No Response
    i2 = (108, 167, 84)     #Offered Interviewed
    i3 = (222, 103, 104)    #Denied

    outer_colors = [o1, o2, o3]
    outer_data = [no_response, offered_interview, denied_before]
    outer_labels = [' No Response: '+str(no_response), 'Offered\nInterview: '+str(offered_interview), 'Early\nRejection: '+str(denied_before)]
    #outer_labels = ['No Response', 'Offered\nInterview', 'Early Rejection']
    print(outer_data)

    inner_colors = [i1, i1, i2, i3, i3]
    inner_data = [no_response, declined_interview, offered, denied_after, denied_before]
    #inner_labels = ['', 'Declined Interview', 'Offered Job', 'Denied', '']
    print(inner_data)

    an = length*3
    make_pie(outer_data, "", outer_colors, outer_labels, radius=1.3, angle=an, explode=(0,0,0))
    make_pie(inner_data, "", inner_colors, ['','','','',''], radius=1.1, angle=an, explode=(0,0,0,0,0))

    #=============================

    ax = plt.gca()


    in_lab = ["Declined Interview", "Job Offer" , "Rejection"]
    inner_crap = []
    for i in range(3):
        if(inner_data[i+1] > 1):
            in_lab[i] += 's'
        inner_crap.append(str(inner_data[i+1]) + ' ' + in_lab[i])

    ax.legend(inner_crap,

            title="Total Applications: "+str(length-1)+"\n   Interview Offers: "+str(sum(inner_data[1:4])),
            #loc="center",frameon=False)
            loc="center",frameon=False)
            #bbox_to_anchor=(1, 0, 0.5, 1))

    plt.show()

def make_pie(sizes, text,colors,labels, radius=1, angle = 0, explode = (0)):
    col = [[i/255 for i in c] for c in colors]

    plt.axis('equal')
    width = 0.35
    kwargs = dict(colors=col, startangle=angle)
    outside, _ = plt.pie(sizes, explode=explode,radius=radius, pctdistance=1-width/2,labels=labels,**kwargs)
    plt.setp(outside, width=width, edgecolor='white')

    kwargs = dict(size=20, fontweight='bold', va='center')
    plt.text(0, 0, text, ha='center', **kwargs)

def main():
    plot(read_sheet())

if __name__ == "__main__":
    main()
