# DriveDB

A tool for those on a job hunt.

Allows you to view and track the status of your applications.

Uses Google Drive as a psuedo-database.

Reads data from a Google Sheet, and outputs a double-donut chart with colored categories.

Uses oauth2, gspread, matplotlib, and numpy.

## Author

Noah Tigner

nzt@cs.uoregon.edu
